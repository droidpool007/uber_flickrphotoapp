# FlickrApp

#### App overview
  - MVVM architecture is used
  - Scrollable list is generated using RecyclerView with GridLayoutManager
  - Network call is made using HttpURLConnection via AsyncTask in ViewModel
  - Multiple api calls are restricted for search & bottom scroll

##### 3rd party if allowed, would had used
  - Retrofit for network call
  - Glide for fetching images
